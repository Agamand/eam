require('../../index.js');
var path = require('path');
var zlib = require('zlib');

var Xml2JSON = Core.include('AgmdJS.tools.Xml2JSON');

var xmlPath = path.normalize(__dirname + '/xml');
var outPath = path.normalize(__dirname + '/json');

var fs = Core.Fs.fs,
	async = Core.Async;

var xmlFiles = {},
	tasks = [];

var DBConnector = Core.Database.getCurrentConnector();


var DbMapping = {
	"eve-blueprints-en-US": "eve.blueprints",
	"eve-certificates-en-US": "eve.certificates",
	"eve-geography-en-US": "eve.geography",
	"eve-items-en-US": "eve.items",
	"eve-masteries-en-US": "eve.masteries",
	"eve-properties-en-US": "eve.properties",
	"eve-reprocessing-en-US": "eve.reprocessing",
	"eve-skills-en-US": "eve.skills",
}

var processFile = function(xmlFile, callback) {
	//
	var type = path.basename(xmlFile.outFile, path.extname(xmlFile.outFile));
	var stream;
	if (xmlFile.gzipfilename) {
		//process gzip file
		Logger.info('process', xmlFile.gzipfilename, type, !!processJSON[type]);
		var inp = fs.createReadStream(xmlPath + '/' + xmlFile.gzipfilename);
		var gunzip = zlib.createGunzip();
		stream = inp.pipe(gunzip);
	} else if (xmlFile.filename) {

		//process xml file
		Logger.info('process', xmlFile.filename);
		stream = fs.createReadStream(xmlPath + '/' + xmlFile.filename);
	} else {
		//problem with formats 
		Logger.info('no file to process');
		callback();
		return;
	}
	Xml2JSON.toJSON(stream, function(err, result) {
		Logger.info('err :', err, 'result :', !!result);
		if (result && processJSON[type]) {
			async.series([function(cb) {
				DBConnector.dropCollection('eam', DbMapping[type], cb)
			}, function(cb) {
				DBConnector.insert('eam', DbMapping[type], processJSON[type](result), cb)
			}], callback);
		} else callback();
	});

}
Core.Fs.makePathSync(outPath + '/test.json');
fs.readdir(xmlPath, function(err, files) {
	for (var i in files) {
		if (/.*\.(gzip|xml)$/.test(files[i])) {

			var file = files[i].split('.');
			var filename = files[i];
			if (file[file.length - 1] == 'gzip') {
				file.pop();
				filename = file.join('.');
				xmlFiles[filename] = xmlFiles[filename] || {}
				xmlFiles[filename].gzipfilename = files[i];
			} else {
				xmlFiles[filename] = xmlFiles[filename] || {}
				xmlFiles[filename].filename = filename;
			}
			file.pop();
			file.push('json');
			xmlFiles[filename].outFile = file.join('.');

		}
	}
	/*

	tasks.push((function(filename) {

					return function(cb) {

						processFile(filename, cb);
					}

				})(files[i]));

	*/
	for (var file in xmlFiles) {
		tasks.push((function(filename) {

			return function(cb) {

				processFile(filename, cb);
			}

		})(xmlFiles[file]));
	}

	async.series(tasks, function() {
		Logger.info('end process');
		process.exit(0);
	})
})

var collect = function(data, tag) {
	var result = [];
	_collect(data, tag, result);
	return result;
}

var _collect = function(data, tag, r, parent) {
	//console.log(parent, typeof parent);
	if (!data)
		return;
	if (data instanceof Array) {
		for (var i in data) {
			_collect(data[i], tag, r, parent);
		}
	} else if (tag[data.tag]) {
		if (parent && parent.length) {
			data.attrs.parents = Core.copy(parent);
		}
		if (data.attrs.id)
			data.attrs.id = +data.attrs.id;
		r.push(data);
	} else if (data.children) {
		parent = parent || [];
		//console.log(data.attrs, data.attrs.name);
		if (data.attrs && data.attrs.name) {
			var p = {};
			p.name = data.attrs.name;
			if (data.attrs.id)
				p.id = +data.attrs.id;
			parent.push(p);
		}
		_collect(data.children, tag, r, parent);
		parent.pop();
	}
}


var formatJSON = {
	"blueprint": function(data) {
		for (var i in data) {
			var orig = data[i],
				res = orig.attrs || {};
			for (var j in orig.children) {
				var c = orig.children[j];
				if (c.tag == "m") {
					if (c.attrs.activityId == "1") {
						res.manufacture = res.manufacture || {};
						res.manufacture.material = res.manufacture.material || {};
						res.manufacture.material[c.attrs.id] = +c.attrs.quantity;
					} else if (c.attrs.activityId == "8") {
						res.invention = res.invention || {};
						res.invention.material = res.invention.material || {};
						res.invention.material[c.attrs.id] = +c.attrs.quantity;
					}
				} else if (c.tag == "s") {
					if (c.attrs.activity == "1") {
						res.manufacture = res.manufacture || {};
						res.manufacture.skill = res.manufacture.skill || {};
						res.manufacture.skill[c.attrs.id] = +c.attrs.lv;
					} else if (c.attrs.activity == "8") {
						res.invention = res.invention || {};
						res.invention.skill = res.invention.skill || {};
						res.invention.skill[c.attrs.id] = +c.attrs.lv;
					}
				} else if (c.tag == 'inventTypeIDs') {
					for (var k in c.children) {
						res.invention = res.invention || {};
						res.invention.result = res.invention.result || {};
						res.invention.result[c.children[k].text] = +c.children[k].attrs.double;
					}
				}
			}
			data[i] = res;
		}
		return data;
	},
	"item": function(data) {
		for (var i in data) {
			var orig = data[i],
				res = orig.attrs || {};
			for (var j in orig.children) {
				var c = orig.children[j];
				if (c.tag == "s") {
					res.skill = res.skill || {};
					res.skill[c.attrs.id] = c.attrs.lv;
				} else if (c.tag == 'p') {
					res[c.tag] = res[c.tag] || {};
					res[c.tag][c.attrs.id] = c.attrs.value;
				} else {
					res[c.tag] = res[c.tag] || {};
					res[c.tag][c.attrs.id] = c.attrs;
				}
			}
			//res.attrs = orig.children;
			data[i] = res;
		}
		return data;
	},
	"reprocess": function(data) {
		for (var i in data) {
			var orig = data[i],
				res = orig.attrs || {};
			for (var j in orig.children) {
				var c = orig.children[j];
				if (c.tag == "material") {
					res.material = res.material || {};
					res.material[c.attrs.id] = +c.attrs.quantity;
				}

			}
			data[i] = res;
		}
		return data;
	},
	"skill": function(data) {
		for (var i in data) {
			var orig = data[i],
				res = orig.attrs || {};
			for (var j in orig.children) {
				var c = orig.children[j];
				if (c.tag == "prereq") {
					res.prereq = res.prereq || {};
					res.prereq[c.attrs.id] = +c.attrs.level;
				}

			}
			data[i] = res;
		}
		return data;
	},
	"systems": function(data) {
		var tags = {};
		for (var i in data) {
			var orig = data[i],
				res = orig.attrs || {};
			for (var j in orig.children) {
				var c = orig.children[j];
				if (c.tag == "prereq") {
					res.prereq = res.prereq || {};
					res.prereq[c.attrs.id] = +c.attrs.level;
				}

			}
			var stations = collect(orig.children, {
				stations: 1
			});
			if (stations && stations.length)
				res.stations = formatJSON['station'](stations, tags);
			data[i] = res;
		}
		//Logger.debug('stations tags', tags);
		return data;
	},
	station: function(data, tags) {

		for (var i in data) {
			var orig = data[i],
				res = orig.attrs;
			for (var j in orig.children) {
				var c = orig.children[j];
				/*if (c.tag == "prereq") {
					res.prereq = res.prereq || {};
					res.prereq[c.attrs.id] = +c.attrs.level;
				}*/
				if (c.tag == 'reprocessingEfficiency')
					res.reprocessingEfficiency = +c.text;
				else if (c.tag == 'reprocessingStationsTake')
					res.reprocessingStationsTake = +c.text;
				else if (c.tag == 'agents') {
					res.agents = res.agents || [];
					for (var k in c.children)
						res.agents.push(c.children[k].attrs);
					//Logger.debug(res.agents);
				}

				if (c.tag)
					tags[c.tag] = 1;

			}
			data[i] = res;
		}

		return data;
	},
	property: function(data) {


		for (var i in data) {
			var res = {};

			var o = data[i];
			for (var j in o.children) {
				var c = o.children[j];
				var v = c.text;
				if (+v + "" == v)
					v = +v;
				res[c.tag] = v
			}
			data[i] = res;
		}

		return data;
	}
}

var processJSON = {

	"eve-blueprints-en-US": function(data) {

		return formatJSON['blueprint'](collect(data, {
			blueprint: 1
		}));
	},
	"eve-items-en-US": function(data) {
		return formatJSON['item'](collect(data, {
			item: 1
		}));
	},
	"eve-reprocessing-en-US": function(data) {
		return formatJSON['reprocess'](collect(data, {
			item: 1
		}));
	},
	"eve-skills-en-US": function(data) {
		return formatJSON['skill'](collect(data, {
			skill: 1
		}));
	},
	"eve-geography-en-US": function(data) {
		return formatJSON['systems'](collect(data, {
			systems: 1
		}));
	},
	"eve-properties-en-US": function(data) {
		return formatJSON['property'](collect(data, {
			property: 1
		}));
	},
}