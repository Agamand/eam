require('../../index.js');
var path = require('path');
var zlib = require('zlib');

var Xml2JSON = Core.include('AgmdJS.tools.Xml2JSON');

var sqlitePath = path.normalize(__dirname + '/sqlite/universeDataDx.db');

var fs = Core.Fs.fs,
	async = Core.Async;

var sqlite = Core.Service.getInstance('AgmdJS.database.SQLiteDatabase', sqlitePath);
var solarSystem = {}
var DBConnector = Core.Database.getCurrentConnector();
sqlite.sql('SELECT solarSystemID,x,y,z FROM mapSolarSystems;', function(err, data) {
	if (err)
		Logger.error('sql error', err);
	else if (!data) {
		Logger.info('update DB');
		async.series([function(cb) {
			DBConnector.find('eam', 'eve.geography', {
				project: {
					_id: 1,
					id: 1
				}
			}, function(err, cursor) {
				var next = function() {

					cursor.nextObject(function(err, data) {
						if (err || !data) {
							cb();
							return;
						}
						var d = data,
							d2 = solarSystem[d.id];
						var set = {
							$set: {
								x: d2.x,
								y: d2.y,
								z: d2.z
							}
						}
						
						DBConnector.updateOne('eam', 'eve.geography', {
							_id: d._id
						}, set, next);
					});
				};
				next();
			})

		}], function() {
			process.exit(0);
		});
	} else {
		solarSystem[data.solarSystemID] = data;
	}
})