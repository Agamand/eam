require('../../index.js');
var path = require('path');
var zlib = require('zlib');

var Xml2JSON = Core.include('AgmdJS.tools.Xml2JSON');

var fs = Core.Fs.fs,
	async = Core.Async;

var itemMap = {},
	propertiesMap = {};
var DBConnector = Core.Database.getCurrentConnector();



async.series([function(cb) {
		//first load properties
		DBConnector.find('eam', 'eve.properties', function(err, cursor) {
			cursor.each(function(err, data) {
				if (err || !data) {
					cb(err);
					return;
				}
				propertiesMap[data.id] = data;
			})
		})
	}, function(cb) {
		//load items
		DBConnector.find('eam', 'eve.items', function(err, cursor) {
			var next = function() {
				cursor.nextObject(function(err, data) {
					if (err || !data) {
						cb();
						return;
					}
					i
					if (data.p) {
						var newP = {};
						for (var j in data.p) {
							if (propertiesMap[j]) {

								var k = propertiesMap[j].name;
								if (newP[k] != undefined) {
									var suffix = 0;
									while (newP[k + (suffix)] != undefined) suffix++;
									newP[k + (suffix)] = data.p[j];
								} else newP[k] = data.p[j];
							} else {
								newP[j] = data.p[j];
								console.log(j);
							}
						}
						delete data.p;
						data.attributes = newP;
					}
					itemMap[data.id] = data;
					DBConnector.updateOne('eam', 'eve.items', {
						_id: data._id
					}, itemMap[data.productTypeID], function() {
						process.nextTick(next);
					});
				});
			};
			next();
		})
	},
	function(cb) {
		DBConnector.find('eam', 'eve.blueprints', function(err, cursor) {
			var next = function() {
				cursor.nextObject(function(err, data) {
					if (err || !data) {
						cb();
						return;
					}
					itemMap[data.productTypeID].createdBy = data.id;
					itemMap[data.id] = Core.apply(itemMap[data.id], Core.apply(data, itemMap[data.id]));
					DBConnector.updateOne('eam', 'eve.items', {
						_id: itemMap[data.productTypeID]._id
					}, itemMap[data.productTypeID], function() {
						DBConnector.updateOne('eam', 'eve.items', {
							_id: itemMap[data.id]._id
						}, itemMap[data.id], function() {
							process.nextTick(next);
						});
					});
				});
			};
			next();

		})
	}
], function() {
	Logger.info('Process ended !');
	process.exit(0);
	sba
});