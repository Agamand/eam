var path = require('path'),
	url = Core.Route.url,
	action_url = Core.Route.action_url,
	res = Core.Route.resource;
Core.configure({
	namespace: {
		'EAM': path.normalize(__dirname + '/../')
	},
	port: 9000,
	pathPrefix: 'EAM',
	routing: [
		//['//test//hello.html', url('/hello.html')],
		['//res//agmdjs//%^(.*)$', res('AgmdJS/res/$0')],
		['//res//eam//%^(.*)$', res('EAM/res/$0')],
		['//index', url('html.Index')],
		['', url('html.Index')],
		['//manufacturing', url('html.industry.Manufacturing')],
		['//agmdjs//%^(.*)$', action_url('AgmdJS/$0')],

		//['//test//%^([1-9])+.html$', url('/super/$0.html')]
	],
	cache: {
		cacheDIR: path.normalize(__dirname + '/../../webcache/'),
		defaultCacheDuration: 3600
	},
	angularJS: {
		application: 'eam'
	},
	database: {
		mongodb: {
			host: '127.0.0.1'
		}
	}
});