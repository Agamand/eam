var path = require('path');
Core.define({
	extendOf: "AgmdJS.database.JSONDatabase",
	JSONPath: path.normalize(__dirname + '/data/eve-skills-en-US.json'),
	ctor: function() {
		var me = this;
		me.loadFile();
	}
});