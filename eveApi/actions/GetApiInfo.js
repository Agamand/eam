/*


test API

one char
1469804
Verification Code: P0R9vLBSqA2MpvEDv6PtWn1fxXeTLFpkiqTQXa8KCUE1yVoYTKnEsshUCbnrMH8W

multi char

1469803
Verification Code: ZdZGuLOWu7At2BLUDJcL65552Q8BeLHGQqOLVEcPVsow36Laovy46G5soVYnA7z9

*/

Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this,
			data = {
				success: false
			};
		data.info = {
			_GET: req._GET
		};
		var api = {};
		try {
			api.keyID = req._GET.keyID;
			api.vCode = req._GET.vCode;
			if (!api.keyID || !api.vCode)
				throw new Error('fail args');
		} catch (e) {
			me.error(e);
			return;
		}

		var eveApiService = Core.Service.getInstance('EAM.eveApi.service.EveApiService');
		me.info('eveApiService loaded', eveApiService);
		eveApiService.getApiInfo({
			api: api
		}, null, function(err, result) {
			me.success(result,result);
		});
		

		//resp.end(JSON.stringify(data));
	}
})