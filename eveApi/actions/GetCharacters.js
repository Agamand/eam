Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this,
			data = {
				success: false
			};
		data.info = {
			_GET: req._GET
		};
		Logger.debug(data);
		var key,vCode;
		try {
			key = req._GET.key,
			vCode = req._GET.vCode;
			if(!key || !vCode)
				throw new Error('fail args');
		}catch(e)
		{
			me.error(e);
			return;
		}

		var eveApiService = Core.Service.getInstance('EAM.eveApi.service.EveApiService');
		Logger.info('eveApiService loaded',eveApiService);

		me.success(data);

		//resp.end(JSON.stringify(data));
	}
})