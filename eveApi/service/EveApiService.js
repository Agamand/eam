Core.define({
	extendOf: "AgmdJS.service.Service",
	devMode: true,
	url: {
		baseURL: 'https://api.eveonline.com',
		baseDevURL: 'https://api.testeveonline.com',
		publicCrest: 'https://public-crest.eveonline.com'
	},
	xmlMethods: {
		APIInfo: "/account/APIKeyInfo.xml.aspx",
	},
	pcMethods: {
		industrySystem: '/industry/systems/',
		wars: '/wars/'
	},

	ctor: function(cfg) {
		var me = this;
		me.loadMethods(__dirname);
		me.baseURL = me.url[me.devMode ? "baseDevURL" : "baseURL"];
		Logger.debug('publicCrestMethods', me.pcMethods);


	}

})