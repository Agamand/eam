var CacheSystem = Core.include('AgmdJS.cache.JSONCache'),
	CacheStatus = CacheSystem.cacheStatus,
	async = Core.Async;

var cacheRequest = function(url, cachename, callback /* callback[err, res] */ ) {
	var me = this,
		jsonData;

	async.series([function(cb) {
		CacheSystem.getCache(cachename, function(err, cache) {
			if (!err && cache && cache.cacheStatus == CacheStatus.GOOD) {
				jsonData = cache.data;
				cb('OK')
			} else cb();
		})
	}, function(cb) {
		Core.Request.request(url, {
			headers: {
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
			}
		}, function(err, req, res) {
			if (err || res.statusCode != 200) {
				me.error("request err", err);
				callback(err);
				return;
			}
			var buffers = [];
			res.on('data', function(chunk) {
				buffers.push(chunk);
			});
			res.on('end', function() {
				var data = Buffer.concat(buffers);
				jsonData = JSON.parse(data.toString());

				CacheSystem.saveCache(cachename, jsonData, {
					duration: 3600
				}, function() {
					cb();
				})
			})
		});
	}], function() {
		callback(null, jsonData);
	})
};



module.exports = cacheRequest;