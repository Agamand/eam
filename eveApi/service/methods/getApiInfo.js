var Xml2JSON = Core.include("AgmdJS.tools.Xml2JSON");
var getApiInfo = function(data, opt, callback) {

	var me = this;

	var url = [me.baseURL, me.xmlMethods.APIInfo];
	url.push('?keyID=', data.api.keyID);
	url.push('&vCode=', data.api.vCode);
	Core.Request.request(url.join(''), {
		headers: {
			"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		}
	}, function(err, req, res) {
		if (err || res.statusCode != 200) {
			me.error("request err", err);
			callback(err);
			return;
		}
		me.info(res.statusCode);
		me.info(Xml2JSON);
		var buffers = [];
		res.on('data', function(chunk) {
			buffers.push(chunk);
		});
		res.on('end', function() {
			var data = Buffer.concat(buffers);
			Helper.parse(data, callback);
		})
	});


}


var Helper = {
	parse: function(xml, callback) {
		Xml2JSON.toJSON(xml, function(err, json) {
			if(!json || !json.length)
			{
				callback();
				return;
			}
			json = json[0];
			var subData = json.children[1].children[0],
				characters = subData.children[0].children;
			var result = {
				accessMask:+subData.attrs.accessMask,
				type:subData.attrs.type,
				expire:subData.attrs.expire,
				version:+json.attrs.version,
				cachedUntil:new Date(json.children[2].text)
			};

			result.characters = [];
			for(var i in characters)
			{
				result.characters.push(characters[i].attrs);
			}
			callback(null,result);
		});
	}
}

module.exports = getApiInfo;