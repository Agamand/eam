var CacheSystem = Core.include('AgmdJS.cache.JSONCache'),
	CacheStatus = CacheSystem.cacheStatus,
	async = Core.Async;

var getCostIndexMap = function(data, opt, callback /* callback[err, res] */ ) {
	var me = this,
		jsonData;
	async.series([
		function(cb) {
			CacheSystem.getCache('costindexmap', function(err, cache) {
				if (!err && cache && cache.cacheStatus == CacheStatus.GOOD) {
					jsonData = cache.data;
					cb('OK')
				} else cb();
			})
		},
		function(cb) {
			me.getCostIndex(null, null, function(err, res) {
				if (res && res.items) {
					jsonData = {};
					for (var i in res.items) {
						var it = res.items[i];
						jsonData[it.solarSystem.id] = it;
					}
					CacheSystem.saveCache('costindexmap', jsonData, {
						duration: 3600
					}, function() {
						cb();
					})
				} else cb();
			})
		}
	], function() {
		callback(null, jsonData);
	})
};


module.exports = getCostIndexMap;