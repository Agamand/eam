var CacheSystem = Core.include('AgmdJS.cache.JSONCache'),
	CacheStatus = CacheSystem.cacheStatus,
	async = Core.Async;

var getWars = function(data, opt, callback /* callback[err, res] */ ) {
	var me = this,
		url = [me.url.publicCrest, me.pcMethods.wars].join('');

	me.cacheRequest(url, 'wars', callback);
};


module.exports = getWars;