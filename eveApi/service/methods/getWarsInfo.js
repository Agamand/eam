var CacheSystem = Core.include('AgmdJS.cache.JSONCache'),
	CacheStatus = CacheSystem.cacheStatus,
	async = Core.Async;

var getWarsInfo = function(data, opt, callback /* callback[err, res] */ ) {
	var me = this;
	if (!data) {
		callback();
		return;
	}

	var url = [me.url.publicCrest, me.pcMethods.wars, data, '/'].join('');

	me.cacheRequest(url, 'wars.' + data, callback);
};


module.exports = getWarsInfo;