Core.define({
	requires: ['EAM.html.SharedFragments'],
	extendOf: 'AgmdJS.template.Template',
	alias: 'main_index',
	fragments: {
		main_header: 1,
		main_index: 1,
		//main_footer: 1
	},
	view: function(ctx, buffer, callback) {

		buffer.write([
			'<div class="container">',
			'</div>'
		].join(''));
		callback();
	}
});