Core.define('EAM.html.Header', {
	singleton: true,
	extendOf: 'AgmdJS.template.Fragment',
	alias: 'main_header',
	model: function(ctx, callback) {

		var me = this;

		ctx.addScript('/res/eam/js/eam.js');
		ctx.mainMenu = {
			setActiveMenu: function(id) {
				console.log('setActiveMenu', id);
				id = id.split('.');
				var tmp = ctx.mainMenu.menu;
				for (var i in id) {
					console.log('active', id[i], tmp);
					if (tmp && tmp[id[i]]) {
						tmp[id[i]].active = true;
						tmp = tmp[id[i]].menu;
					} else return;
				}
			},
			menu: {
				tools: {
					href: '/tools',
					title: 'Tools',
				},
				nav: {
					title: 'Navigation',
					menu: {
						map: {
							title: 'Map',
							href: '/map'
						},
						route: {
							title: 'Route Planner',
							href: '/route'
						},
						jump: {
							title: 'Jump Planner',
							href: '/jump'
						}
					}
				},
				fit: {
					title: 'Fittings',
					menu: {
						map: {
							title: 'Search fit',
							href: '/searchfit'
						},
						route: {
							title: 'Create fit',
							href: '/createfit'
						}
					}
				},
				industry: {
					title: 'Industry',
					menu: {
						manufacturing: {
							title: 'Manufacturing',
							href: '/manufacturing'
						},
						invention: {
							title: 'Invention',
							href: '/invention'
						},
						mining: {
							title: 'Mining Optimizer',
							href: '/mining'
						}
					}
				}
			}
		}
		me.debug("user",ctx.req._USER);
		//process.exit(0);
		if (ctx.req._USER) {
			ctx.mainMenu.menu.profile = {
				title: 'Profile',
				position: 'right',
				menu: {
					settings: {
						title: 'Settings',
						href: '#'
					},
					logout: {
						title: 'Logout',
						action: 'EAM.account.Logout'
					}
				}
			}
		} else {
			ctx.mainMenu.menu.login = {
				title: 'Login',
				position: 'right',
				action: 'EAM.account.Login'
			}
		}

		Logger.debug('header model', ctx.menu);

		callback();
	},
	view: function(ctx, buffer, callback) {
		//buffer.write([
		var _buffer = [];
		_buffer.push(
			'<div class="navbar navbar-default navbar-static-top navbar-inverse" id="top" role="banner">',
			'<div class="container">',
			'<div class="navbar-header">',
			'<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-main-navbar" aria-controls="bs-navbar" aria-expanded="false">',
			'<span class="sr-only">Toggle navigation</span>',
			'<span class="icon-bar"></span>',
			'<span class="icon-bar"></span>',
			'<span class="icon-bar"></span>',
			'</button>',
			'<a href="../" class="navbar-brand">EAM</a>',
			'</div>'
		);
		_buffer.push(
			'<nav class="collapse navbar-collapse" id="bs-main-navbar">'
		);

		Logger.debug(ctx.mainMenu);
		if (ctx.mainMenu) {
			var left = [],
				right = [];

			for (var i in ctx.mainMenu.menu) {
				var cur = ctx.mainMenu.menu[i];
				console.log('add menu', cur)
				var menuBuffer = cur.position == 'right' ? right : left;
				if (cur.menu) {
					//it's dropdown menu


					console.log('add dropdown')
					menuBuffer.push(
						'<li class="dropdown', cur.active ? ' active' : '', '">',
						'<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">', cur.title,
						'<span class="caret"></span>',
						'</a>',
						'<ul class="dropdown-menu">'
					);
					for (var j in cur.menu) {
						var scur = cur.menu[j];
						if (scur.sep) {
							menuBuffer.push('<li role="separator" class="divider"></li>');
						} else {
							menuBuffer.push(
								'<li', scur.active ? ' class="active"' : '', '>',
								'<a href="', scur.href, '">', scur.title, '</a>',
								'</li>'

							);
						}
					}
					menuBuffer.push('</ul></li>');
				} else {
					//simple menu
					menuBuffer.push('<li', cur.active ? ' class="active"' : '', '>');
					if (cur.href)
						menuBuffer.push('<a href="', cur.href, '">', cur.title, '</a>');
					else if (cur.action)
						menuBuffer.push('<a href="#" action="', cur.action, '">', cur.title, '</a>')
					menuBuffer.push('</li>');
				}
			}
			if (left && left.length)
				_buffer.push('<ul class="nav navbar-nav navbar-left">', left.join(''), '</ul>')
			if (right && right.length)
				_buffer.push('<ul class="nav navbar-nav navbar-right">', right.join(''), '</ul>')
		}

		_buffer.push(
			'</nav>',
			'</div>',
			'</div>'
		);

		buffer.write(_buffer.join(''));
		callback();
	}
});

Core.define('EAM.html.Footer', {
	singleton: true,
	extendOf: 'AgmdJS.template.Fragment',
	alias: 'main_footer',
	view: function(ctx, buffer, callback) {
		buffer.write([''].join(''));
		callback();
	}
});