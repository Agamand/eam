var BootstrapHelper = Core.include('AgmdJS.tools.HtmlBootstrapHelper'),
	HtmlBuilder = Core.include('AgmdJS.tools.HtmlBuilder');

Core.define({
	requires: ['EAM.html.SharedFragments'],
	extendOf: 'AgmdJS.template.Template',
	alias: 'manufacturing',
	fragments: {
		main_header: 1,
		manufacturing: 1,
		main_footer: 1
	},
	model: function(ctx, callback) {
		var me = this;
		ctx.mainMenu.setActiveMenu('industry.manufacturing');
		me.loadFileScript(__dirname + '/clientJS/manufacturing.js', function(err, file) {
			ctx.addInlinesScripts(file);
			callback();
		})

	},
	view: function(ctx, buffer, callback) {

		var _buffer = [];
		_buffer.push(
			'<div class="container">',
			'<div class="panel panel-default">',
			'<div class="panel-heading">',
			'<h3 class="panel-title">Production</h3>',
			'</div>',
			'<div class="panel-body">',
			'<form role="form">',
			'<input id="item-autocompleter" autocomplete="off" type="text">',
			'<button id="add-item" type="button" class="btn btn-default">Add</button>',
			'</form>'
			//'<table><thead><tr></tr></thead></table>'
		);



		var productionHeader = ['Name', 'ME', 'TE', 'ME Modifier', 'TE Modifier', 'Job count', 'Run count', 'Job Cost', '']


		var rows = [
			['Ishtar', 0, 0, 1, 1, '1 ISK', '<span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:red; cursor:click;"></span>']
		];
		BootstrapHelper.table('production-table', productionHeader).toHtml(_buffer);
		_buffer.push(
			'</div>',
			'</div>',
			'<div class="panel panel-default">',
			'<div class="panel-heading">',
			'<h3 class="panel-title">Materials</h3>',
			'</div>',
			'<div class="panel-body">'
		);
		var materialsHeader = ['Name', 'Count', 'Price', 'Unit Price', '']
		BootstrapHelper.table('materials-table', materialsHeader).toHtml(_buffer);
		_buffer.push(
			'</div>',
			'</div>',
			'<div class="panel panel-default">',
			'<div class="panel-heading">',
			'<h3 class="panel-title">Result</h3>',
			'</div>',
			'<div class="panel-body">');
		BootstrapHelper.table('result-table', ['name', 'count', 'prices']).toHtml(_buffer);
		_buffer.push(
			'</div>',
			'</div>',
			'<div class="panel panel-default">',
			'<div class="panel-heading">',
			'<h3 class="panel-title">Prices</h3>',
			'</div>',
			'<div class="panel-body">',
			'<form role="form">',
			'<button id="update-price" type="button" class="btn btn-default">Update Price</button>',
			'</form>',
			'<div class="row">',
			'<label>Job Price</label>', '<span id="job-price"></span>',
			'</div><div class="row">',
			'<label>Materials Price</label>', '<span id="material-price"></span>',
			'</div><div class="row">',
			'<label>Blueprint Price</label>', '<span id="bp-price"></span>',
			'</div><div class="row">',
			'<label>Total Price</label>', '<span id="total-price"></span>',
			'</div>',
			'</div>');
		buffer.write(_buffer.join(''));


		var panel = HtmlBuilder.BootstrapPanel({
			title: ''
		}).append(function() {

			HtmlBuilder.table({
				attrs: {
					id: 'testTable',
					'ng-controller':'testCtrl',
					'class': 'table table-striped'
				}
			}, this).appendHeaders(function() {
				var head = ['1', '2', '3', '4'];

				HtmlBuilder.elt('tr', this).append(function() {
					for (var i in head) {
						HtmlBuilder.elt('th', this).appendText(head[i]);
					}
				});


			}).appendRows(function() {
				var row = ['{{ d.a }}', '{{ d.b }}', '{{ d.c }}', '{{ d.d }}']
				HtmlBuilder.elt('tr', {
					attrs: {
						'ng-repeat': 'd in data'
					}
				}, this).append(function() {
					for (var i in row) {
					HtmlBuilder.elt('td', this).appendText(row[i]);
					}
				});
			});


		});

		buffer.write(panel.getBuffer().toString());

		buffer.write('</div>');

		callback();
	}
});