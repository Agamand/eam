var g = this,
	async = Core.Async;
g.Wrapper = {
	items: {},
	itemsbid: {},
	bps: {},
	production: {},
	materials: {},
	prices: {
		job: 0,
		material: 0,
		bp: 0,
		total: 0
	},
	systems: {
		amarr: 30002187,
		jita: 30000142
	},
	//productionTable: $('#production-table tbody'),
	//materialTable: $('#material-table tbody'),
	productionTags: ['id', 'te', 'me', 'me_modifier', 'te_modifier', 'job_count', 'run_count'],
	init: function() {

	},
	getItem: function(ids, callback) {
		$.getJSON(['/search/actions/GetItemById?id=', ids.join(';')].join(''), function(result) {
			if (result && result.success) {
				callback(null, result.data)
			} else callback();
		});
	},
	input: function(val, styles, id) {
		return ['<input id="', id, '" type="text" style="', styles, '" value="', val, '">'].join('');
	},
	generateProdutionTable: function() {
		var me = this;
		me.productionTable = me.productionTable || $('#production-table tbody')
		var _buffer = [],
			idx = 0,
			styles = 'width: 40px;';
		for (var i in me.production) {
			var d = me.production[i];
			var row = [d.name, me.input(d.me, styles, 'me'), me.input(d.te, styles, 'te'), me.input(d.me_modifier, styles, 'me_modifier'), me.input(d.te_modifier, styles, 'te_modifier'), me.input(d.job_count, styles, 'job_count'), me.input(d.run_count, styles, 'run_count'), me.job_cost(d.id, d.job_count * d.run_count) + ' ISK', '<span id="del-prod" class="glyphicon glyphicon-remove" aria-hidden="true" style="color:red; cursor:pointer;"></span>'];
			d.idx = idx;
			_buffer.push('<tr id="r-', idx++, '">');

			for (var j in row)
				_buffer.push('<td id="c-', j, '">', row[j], '</td>');
			_buffer.push('</tr>');
		}

		me.productionTable.empty();
		me.productionTable.append($(_buffer.join('')));
		me.update('job', 0);
		me.productionTable.find('input').on('change', function(val) {

			var input = $(this),
				tag = input.attr('id'),
				val = input.val(),
				parents = input.parents('tr'),
				frow = parents.find('td#c-0'),
				iname = frow.text(),
				prod = me.production[iname];
			if (!prod)
				return;
			if (+val + "" != val) {
				input.val(prod[tag]);
			} else {
				prod[tag] = +val;
				me.updateMaterials();
				me.updateHash();
			}


		});
		$('span#del-prod').on('click', function() {
			console.log('this ->', this, 'args ->', arguments);
			var elt = $(this);
			var parents = elt.parents('tr'); //.detach();

			var frow = parents.find('td#c-0'),
				iname = frow.text();
			if (me.production[iname]) {
				delete me.production[iname];
				parents.detach();
				me.updateMaterials();
				me.updateHash();
			}
			//console.log('text',frow.text(),frow.html());



		});
	},
	updateRow: function(target, r, c, val) {
		var subtarget = target.find(['tr#r-', r, ' td#c-', c].join(''))

		if (subtarget.find('input').length) {
			subtarget.find('input').val(val);
		} else subtarget.html(val);
	},

	updateResult: function(callback) {
		var me = this;
		me.resultTable = me.resultTable || $('#result-table tbody')
		var _buffer = [],
			idx = 0;
		for (var i in me.production) {
			var d = me.production[i];
			var row = [d.name, d.job_count * d.run_count, 0];
			d.idx = idx;
			_buffer.push('<tr id="r-', idx++, '">');

			for (var j in row)
				_buffer.push('<td id="c-', j, '">', row[j], '</td>');
			_buffer.push('</tr>');
		}

		me.resultTable.empty();
		me.resultTable.append($(_buffer.join('')));
		if (callback)
			callback();
	},

	getMaterialCount: function(value, me, me_modifier, jc, run) {
		return (Math.ceil(Math.max(value * (1 - me / 100) * me_modifier, 1) * run)) * jc;
	},
	serialize: function(data, tags) {
		var _data = [],
			me = this;
		for (var i in data) {
			var subData = [];
			for (var j in tags) {
				subData.push(data[i][tags[j]]);
			}
			_data.push(subData.join(';'));
		}
		return _data.join('\n') || '';
	},
	deserialize: function(data, tags) {
		var res = [],
			me = this
		data = data.split('\n');
		for (var i in data) {
			var _d = data[i].split(';');
			var _data = {};
			for (var j in _d) {
				var v = _d[j];
				if (+v + "" == v)
					v = +v;
				console.log(v, tags[j] || ('unk' + j))
				_data[tags[j] || ('unk' + j)] = v;
			}
			res.push(_data)
		}
		return res;
	},
	updateHash: function() {
		var me = this;

		var serializedData = me.serialize(me.production, me.productionTags)
		var gzip = new Zlib.Gzip(Core.stringToByteArray(serializedData)).compress();
		window.location.hash = Core.Base64.encode(Core.byteArrayToString(gzip));
	},
	loadFromHash: function(callback) {
		var hash = window.location.hash,
			me = this;
		if (hash && hash.length) {


			var compressed = Core.stringToByteArray(Core.Base64.decode(hash));
			var uncompressed = new Zlib.Gunzip(compressed).decompress();
			var data = me.deserialize(Core.byteArrayToString(uncompressed), me.productionTags);
			var ids = [];
			for (var i in data)
				ids.push(data[i].id);
			me.loadItem(ids, function() {
				for (var i in data) {
					var d = data[i];
					var item = me.itemsbid[d.id];
					d.name = item.name;
					d.job_cost = 0;
					me.production[d.name] = d;
				}
				me.generateProdutionTable();
				me.updateMaterials(callback);
			});
		} else callback();
	},
	getMarketPrice: function(itemsids, callback) {
		var me = this;
		if (!itemsids || !itemsids.length) {
			callback();
			return
		}
		if (!(itemsids instanceof Array))
			itemsids = [itemsids]

		//  var url = ['http://api.eve-marketdata.com/api/item_prices2.json?char_name=demo&region_ids=10000002&buysell=s&type_ids=',itemsids.join(',')].join('');
		var url = ['http://api.eve-central.com/api/marketstat/json?usesystem=', me.systems.amarr];
		for (var i in itemsids) {
			url.push('&typeid=', itemsids[i]);
		}
		//return [url.join('')];
		$.getJSON(url.join(''), function(data) {
			var res = {};
			var tags = ['volume', "wavg", "avg", "variance", "stdDev", "median", "fivePercent", "max", "min"];
			if (data) {
				for (var i in data) {
					var o = data[i];
					if (o.sell) {
						var id = o.sell.forQuery.types[0];
						res[id] = res[id] || {};
						res[id].sell = {};
						for (var j in tags)
							res[id]['sell'][tags[j]] = o.sell[tags[j]];
					}
					if (o.buy) {
						var id = o.buy.forQuery.types[0];
						res[id] = res[id] || {};
						res[id].buy = {};
						for (var j in tags)
							res[id]['buy'][tags[j]] = o.buy[tags[j]];
					}
				}
			}
			callback(null, res);
		});
	},
	updatePrice: function() {
		var me = this,
			needed = [];
		for (var i in me.materials) {
			if (me.itemsbid[i] && !me.itemsbid[i].market)
				needed.push(+i);
		}
		me.getMarketPrice(needed, function(err, data) {
			for (var id in data) {
				me.itemsbid[id].market = data[id];
			}
			me.updateMaterials();
		})
	},
	addProduction: function(name, job_count, run_count) {
		var me = this;
		console.log('addProduction', name, 'job_count =', job_count, 'run_count', run_count);
		if (!me.items[name])
			return;
		if (me.production[name]) {
			if (!job_count && !run_count)
				me.production[name].run_count += 1;
			else me.production[name].run_count += job_count * run_count;
			me.updateRow(me.productionTable, me.production[name].idx, 5, me.production[name].job_count);
		} else {
			var d = me.production[name] = {};
			d.id = me.items[name].id;
			d.name = name;
			d.me = d.te = 0;
			d.me_modifier = d.te_modifier = d.job_cost = 1;
			d.job_count = job_count || 1;
			d.run_count = run_count || 1;
			me.generateProdutionTable();
		}
		me.updateMaterials();
		me.updateHash();
	},
	loadBlueprint: function(ids, callback) {
		var me = this;
		if (!ids || !ids.length) {
			callback();
			return;
		}
		if (!(ids instanceof Array))
			ids = [ids];

		me.getItem(ids, function(err, result) {
			if (!result || !result.length) {
				callback();
				return;
			}

			for (var i in result) {
				var r = result[i];
				me.bps[r.id] = r;
			}
			callback();

		})
	},
	loadItem: function(ids, callback) {
		var me = this;
		if (!ids || !ids.length) {
			callback();
			return;
		}
		if (!(ids instanceof Array))
			ids = [ids];

		me.getItem(ids, function(err, result) {
			if (!result || !result.length) {
				callback();
				return;
			}

			for (var i in result) {
				var r = result[i];
				me.itemsbid[r.id] = r;
				me.items[r.name] = r;
			}
			callback();

		})
	},
	addCommas: function(nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	},
	/*

	prices: {
		job: 0,
		material: 0,
		bp: 0,
		total: 0
	},

	*/
	job_cost: function(id) {
		return 0;
	},
	update: function(tag, val) {
		var me = this;
		me.prices[tag] = val;
		$('#' + tag + '-price').text(me.addCommas(me.prices[tag] + ''));
		me.updateTotalPrice();
	},
	updateTotalPrice: function() {
		var me = this;
		me.prices.total = me.prices.job + me.prices.material + me.prices.bp;
		$('#total-price').text(me.addCommas(me.prices.total + ''));

	},
	updateMaterials: function(callback) {
		var me = this,
			needed = [];
		for (var i in me.production) {
			var item = me.items[me.production[i].name];
			if (item.createdBy && !me.bps[item.createdBy])
				needed.push(item.createdBy);
		}
		me.loadBlueprint(needed, function() {
			me.materials = {};
			needed = {};
			for (var i in me.production) {
				var production = me.production[i];
				var item = me.items[production.name];
				if (item.createdBy && me.bps[item.createdBy]) {
					var bp = me.bps[item.createdBy];
					if (bp && bp.manufacture && bp.manufacture.material) {
						var mat = bp.manufacture.material;
						console.log(mat);
						for (var k in mat) {
							//getMaterialCount: function(value, me, me_modifier, jc, run)
							var count = me.getMaterialCount(mat[k], production.me, production.me_modifier, production.job_count, production.run_count);
							me.materials[k] = (me.materials[k] || 0) + count;
							if (!me.itemsbid[k])
								needed[k] = 1;
						}
					}
				}
			}

			var toload = [];
			for (var i in needed)
				toload.push(+i);
			me.loadItem(toload, function(err, result) {
				me.materialTable = me.materialTable || $('#materials-table tbody')
				var _buffer = [],
					idx = 0;
				var total = 0;
				for (var id in me.materials) {
					var item = me.itemsbid[id];
					var d = me.materials[id];

					if (me.production[item.name]) {
						var prod = me.production[item.name];
						prod.comsumed = prod.job_count * prod.run_count - d;
						d -= prod.job_count * prod.run_count;
					}

					if (d <= 0)
						continue;
					var cost = 0,
						unitPrice = 0;
					if (item.market) {
						unitPrice = item.market.buy.max;
						cost = d * unitPrice;

					}
					total += cost;
					var row = [item.name, d, me.addCommas(cost + ""),me.addCommas(unitPrice + ""), item.createdBy ? '<span id="add-prod" class="glyphicon glyphicon-plus-sign" aria-hidden="true" style="color:green; cursor:pointer;"></span>' : ''];
					d.idx = idx;
					_buffer.push('<tr id="r-', idx++, '">');

					for (var j in row)
						_buffer.push('<td id="c-', j, '">', row[j], '</td>');
					_buffer.push('</tr>');
				}
				me.materialTable.empty();
				me.materialTable.append($(_buffer.join('')));

				$('span#add-prod').on('click', function() {
					var elt = $(this);
					var parents = elt.parents('tr'); //.detach();

					var frow = parents.find('td#c-0'),
						count = +parents.find('td#c-1').text(),
						iname = frow.text(),
						item = me.items[iname];
					if (me.materials[item.id]) {
						me.addProduction(item.name, 1, count);
					}
					//console.log('text',frow.text(),frow.html());
				});
				me.update('material', total);
				me.updateResult(callback);
			});
		})

	}


}


$(function() {
	g.Wrapper.loadFromHash(function() {
		$("#item-autocompleter").typeahead({
			source: eve_search_item
		})
		$("#add-item").on('click', function() {
			var item = $("#item-autocompleter").val();
			console.log('val', item);
			if (item && item.length) {
				Wrapper.addProduction(item);
			}
		})
		$("#update-price").on('click', function() {
			Wrapper.updatePrice();
		})
	});
});


g.query = {};
g.eve_search_item = function(query, callback) {
	console.log("eve_search_item", query, callback);
	if (g.query[query]) {
		callback(g.query[query]);
		return;
	}
	$.getJSON("/search/actions/SearchItem?q=" + query, function(data) {
		var res = [];
		//console.log(typeof data,data);,
		for (var i in data.data) {
			var d = data.data[i];
			if (!d.createdBy)
				continue;
			res.push(d.name);
			if (!Wrapper.items[d.name])
				Wrapper.items[d.name] = d;

		}
		g.query[query] = res;
		callback(res);
	});
}