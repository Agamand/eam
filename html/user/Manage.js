var BootstrapHelper = Core.include('AgmdJS.tools.HtmlBootstrapHelper'),
	HtmlBuilder = Core.include('AgmdJS.tools.HtmlBuilder');

Core.define({
	requires: ['EAM.html.SharedFragments'],
	extendOf: 'AgmdJS.template.HtmlTemplate',
	alias: 'usermanager',
	fragments: {
		main_header: 1,
		usermanager: 1,
		main_footer: 1
	},
	model: function(ctx, callback) {
		var me = this;
		ctx.mainMenu.setActiveMenu('industry.manufacturing');
	}
});