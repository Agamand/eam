Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this
		var itemID;
		try {
			itemID = req._GET.itemID;
			if (!itemID)
				throw new Error('fail args');
		} catch (e) {
			me.error(e);
			return;
		}

		var industryService = Core.Service.getInstance('EAM.industry.service.IndustryService');
		me.info('industryService loaded', industryService);
		industryService.getProductionInfo({
			id: itemID
		}, null, function(err, result) {
			me.success(result, result);
		});


		//resp.end(JSON.stringify(data));
	}
})