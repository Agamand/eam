 Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this
		var systemId;
		try {
			systemId = req._GET.id;
			if (!systemId)
				throw new Error('fail args');
		} catch (e) {
			me.error(e);
			return;
		}

		var industryService = Core.Service.getInstance('EAM.industry.service.IndustryService');
		me.info('industryService loaded', industryService);
		industryService.getSystemCostIndex({
			id: systemId
		}, null, function(err, result) {
			me.success(result, result);
		});


		//resp.end(JSON.stringify(data));
	}
})