Core.define({
	extendOf: 'AgmdJS.service.Service',
	database: 'eam',
	collection: {
		blueprints: 'eve.blueprints',
		items: 'eve.items'
	},
	eveApiService: Core.Service.getInstance('EAM.eveApi.service.EveApiService'),
	ctor: function(cfg) {
		var me = this;
		me._super(cfg);
		me.loadMethods(__dirname);
	}

})