var getProductionInfo = function(data, opt, callback) {

	var me = this;

	me.dbConnector.findOne(me.database, me.collection.blueprints, {
		productTypeID: data.id
	}, function(err, data) {


		if (data) {
			var ids = [];
			for (var i in data.manufature.material) {
				ids.push(+i);
			}

			me.dbConnector.find(me.database, me.collection.blueprints, {
				filter: {
					productTypeID: {
						$in: ids
					}
				}
			}, function(err, cursor) {
				if (err || !cursor) {
					callback(err);
					return;
				}
				cursor.toArray(function(err, subdata) {
					callback(err, {
						data: data,
						subdata: subdata
					});
				});

			});
		} else callback(err, data);
	});

}
module.exports = getProductionInfo;