var app = angular.module("eam", []);
app.controller('testCtrl', function($scope, $http) {
	$scope.data = [{
		a: 1,
		b: 2,
		c: 3,
		d: 4
	}, {
		a: 4,
		b: 3,
		c: 2,
		d: 1
	}];
});

g.eam = {}

Core.jsPath['EAM'] = '/res/eam/js/class'


Core.define('EAM.account.Login', {
	extendof: 'AgmdJS.action',
	singleton: true,
	loginHtml: ['<div class="modal fade" id="loginModal" role="dialog">',
		'<div class="modal-dialog">',
		'<div class="modal-content">',
		'<div class="modal-header">',
		'<button type="button" class="close" data-dismiss="modal">&times;</button>',
		'<h4 class="modal-title">Modal Header</h4>',
		'</div>',
		'<div class="modal-body">',
		'<form role="form">',
		'<div class="form-group">',
		'<label for="email">Email address:</label>',
		'<input type="email" class="form-control" id="email">',
		'</div>',
		'<div class="form-group">',
		'<label for="pwd">Password:</label>',
		'<input type="password" class="form-control" id="pwd">',
		'</div>',
		'<div class="checkbox">',
		'<label><input type="checkbox"> Remember me</label>',
		'</div>',
		'<button type="submit" class="btn btn-default">Submit</button>',
		'</form>',
		'</div>',
		'<div class="modal-footer">',
		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>',
		'</div>',
		'</div>',
		'</div>',
		'</div>'
	],
	proceed: function() {
		var me = this;
		var modal = $('#loginModal');
		if (!modal.length)
			$('body').append(me.loginHtml.join(''));
		$('#loginModal').modal('show')
	}
})