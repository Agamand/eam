Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this,
			ids;
		try {
			ids = req._GET.id;
			if (!ids)
				throw new Error('fail args');
		} catch (e) {
			me.error(e);
			return;
		}
		ids = ids.split(';');
		for (var i in ids)
			ids[i] = +ids[i];
		var searchService = Core.Service.getInstance('EAM.search.service.SearchService');
		me.info('SearchService loaded', !!searchService);
		searchService.getItemById(ids, null, function(err, result) {
			me.success(result, result);
		});


		//resp.end(JSON.stringify(data));
	}
})