Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this,
			query;
		try {
			query = req._GET.q;
			if (!query)
				throw new Error('fail args');
		} catch (e) {
			me.error(e);
			return;
		}

		var searchService = Core.Service.getInstance('EAM.search.service.SearchService');
		searchService.searchSystem(query, null, function(err, result) {
			me.success(result, result);
		});


		//resp.end(JSON.stringify(data));
	}
})