Core.define({
	extendOf: "AgmdJS.service.Service",
	database: 'eam',
	collection: {
		blueprints: 'eve.blueprints',
		items: 'eve.items',
		geography: 'eve.geography'
	},
	ctor: function(cfg) {
			var me = this;
			me._super(cfg);
			me.loadMethods(__dirname);
			//me.initIndex();
		}
		/*,
			initIndex: function() {
				var me = this;
				me.si = require('search-index')({
					deletable: true,
					fieldedSearch: true,
					indexPath: 'item_index',
					logLevel: 'error',
					nGramLength: 1,
					//stopwords: SearchIndex.getStopwords('en'),
					fieldsToStore: 'all'
				});
				me.itemDb.find({}, {
					id: 1,
					name: 1
				}).toArray(function(err, data) {
					me.si.add(data, {}, function(err) {
						if (err) console.log('oops! ' + err);
						else console.log('create index success!');
						me.index_init = true;
						me.fireEvent('index_init');
					});
				});

			}*/

})