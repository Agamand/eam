var async = Core.Async;
var getItemById = function(data, opt, callback) {
	var me = this;

	Logger.debug('getItemById', data);
	var res = [];
	async.series(
		[
			function(cb) {

				me.dbConnector.find(me.database, me.collection.items, {
					filter: {
						id: {
							$in: data
						}
					}
				}, function(err, cursor) {
					if (err || !cursor) {
						callback(err);
						return;
					}
					cursor.each(function(err, data) {
						if (err || !data) {
							cb();
							return;
						}
						res.push(data);
					})
				})
			},
			function(cb) {
				me.dbConnector.find(me.database, me.collection.blueprints, {
					filter: {
						id: {
							$in: data
						}
					}
				}, function(err, cursor) {
					if (err || !cursor) {
						callback(err);
						return;
					}
					cursor.each(function(err, data) {
						if (err || !data) {
							cb();
							return;
						}
						res.push(data);
					})
				});
			}
		],
		function(err) {
			callback(err, res);
		}
	);

}
module.exports = getItemById;