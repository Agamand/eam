var searchItem = function(data, opt, callback) {
	var me = this;

	me.dbConnector.find(me.database, me.collection.items, {
		filter: {
			name: new RegExp("^" + data, 'i')
		},
		project: {
			id: 1,
			name: 1,
			createdBy: 1
		}
	}, function(err, cursor) {
		if (err || !cursor) {
			callback(err);
			return;
		}
		cursor.toArray(callback);
	})
}
module.exports = searchItem;