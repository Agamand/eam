var searchSystem = function(data, opt, callback) {
	var me = this;
	me.dbConnector.find(me.database, me.collection.geography, {
		filter: {
			name: new RegExp("^" + data, 'i')
		},
		project: {
			id: 1,
			name: 1
		}
	}, function(err, cursor) {
		if (err || !cursor) {
			callback(err);
			return;
		}
		cursor.toArray(callback);
	});
}
module.exports = searchSystem;