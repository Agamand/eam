require('./index.js')
Core.RoutingTree =Core.Route.createRouteTree(Core.getConfig('routing')) 
Logger.info(Core.getConfig('routing'));	
Core.include('AgmdJS.core.Server', function(err, clazz) {
	new clazz({
		port: Core.getConfig('port') || 80
	}).start();
})