require('../index.js');

var BPdatabase = Core.Service.getInstance('EAM.database.ItemDatabase');

BPdatabase.find({
	'metaGroup': 'T2',
	"category": "Ship",
	"name": /^Ar/i
}, {
	id: 1,
	name: 1
}).sort({
	'name': 1
}).foreach(function(err, data) {
	if (err || !data) {
		Logger.info('cursor ended');
		return;
	}
	Logger.info('data', data.id, data.name);

})