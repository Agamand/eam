require('../index.js'),
	path = require('path'),
	async = Core.Async;
var Geodatabase = Core.Service.getInstance('AgmdJS.database.JSONDatabase', path.normalize(__dirname + '/../database/data/eve-geography-en-US.json'));
var b;
var sys = [];

var dist = function(a, b) {

	var x = a.x - b.x,
		y = a.y - b.y,
		z = a.z - b.z;


	return Math.sqrt(x * x + y * y + z * z);

}

var LY = 9.4605284e15;

async.series([function(cb) {
	Geodatabase.findOne({
		'name': 'Teshkat'
	}, {
		id: 1,
		x: 1,
		y: 1,
		z: 1,
		name: 1
	}, function(err, data) {
		b = data;
		cb();
	})
}, function(cb) {
	var cur = Geodatabase.find({}, {
		id: 1,
		x: 1,
		y: 1,
		z: 1,
		name: 1
	});
	console.log(cur);
	cur.foreach(function(err, data) {
		if (err || !data) {
			cb();
			return;
		}
		var d = dist(data, b) / LY;
		if (d <= 14 && data.id != b.id)
			sys.push({
				d: d,
				name: b.name + ' -> ' + data.name
			})
	})
}], function() {
	sys = sys.sort(function(a, b) {
		return a.d - b.d;
	});
	Logger.debug(sys);
})