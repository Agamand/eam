require('../index.js');
var BPdatabase = Core.Service.getInstance('EAM.database.ItemDatabase');



var SI = require('search-index');
var clazz = Core.define("EAM.TestClass", {
	extendOf: "AgmdJS.service.Service",

	ctor: function() {
		var me = this;
		me.bp = BPdatabase;
		var si = SI({});
		me.s = si;
		me.bp.find({}, {
			id: 1,
			name: 1
		}).toArray(function(err, data) {
			Logger.debug('add to index');
			me.s.add(data, {}, function(err) {

				if (err) console.log('oops! ' + err);
				else console.log('create index success!');
				me.s.search({
					query: {
						'*': ['blabla']
					}
				}, function(err, searchResults) {
					Logger.info(err, searchResults);
				})
			});
		})
	}
});

new clazz();