var mineral = [
	'Tritanium',
	'Pyerite',
	'Mexallon',
	'Isogen',
	'Nocxium',
	'Zydrine',
	'Megacyte'
],
/*init = [
	10408000,
	2603601,
	655500,
	163090,
	40620,
	18580,
	6801
];*/
init = [
	3721567,
	1040048,
	307707,
	56348,
	27367,
	7640,
	3512
];

Core.define({
	fragments: {
		'Header': 1
	},
	extendOf: 'AgmdJS.template.Template',
	model: function(ctx, buffer, callback) {
		ctx.addScript('/res/agmdjs/glpk.min.js');
		ctx.addStyles(['.minerals input {height: 23px;}','.minerals {margin: 11px 0px;}','span.mineral {width: 90px;display: inline-block;}','.result div {min-height: 16px;}'])
		//ctx.addScript('/res/js/')
		callback();
	},
	view: function(ctx, buffer, callback) {

		var text = [];

		text.push('<div style="float:left">')
		for (var i in mineral) {
			text.push('<div class="minerals"><span class="mineral">', mineral[i], '</span><input id="ip',i,'" value="',init[i],'"></input></div>');
		}
		text.push('<div class="minerals"><span class="mineral">Refine rate</span><input id="refineRate" value="0.70"></input></div>');

		text.push('</div><div style="float:left;">')
		text.push('<button class="validate">VALIDATE</button></div>')
		text.push('<div class="result" style="float:left;"></div>')

		buffer.write(text.join(''));
		buffer.write(["<script>",
			"var mineral = ['Tritanium','Pyerite','Mexallon','Isogen','Nocxium','Zydrine','Megacyte'];",
			"Core.include('evejs.OreMinimizer',function(clazz){",
				"console.log(clazz);",
				"var minimizer = new clazz();",
				"minimizer.on('ready',function(){",
					"minimizer.solve();",
				"});",
				"console.log('not ready');",
				"$('.validate').on('click',function(){",
					"var required = [];",
					"for(var i = 0; i < 7; i++){",
						"required.push(+$('#ip'+i).val());",
					"}",
					"required.push(0,0);",
					"var options = {refineRate:+$('#refineRate').val()}",
					"minimizer.solve(required,options,function(err,res){",
						"var str = ['Result :',res.str,'\\nCost : ',res.cost,'\\nCargo required (uncompressed version) : ',res.cargou,'\\nCargo required (compressed version) : ',res.cargo].join('\\n');",
						"str = str.split('\\n');",
						"$('.result').empty();",
						"for(var i in str) $('.result').append(['<div>',str[i],'</div>'].join(''));",
						"$('.result').append(['<div>','differential : ','</div>'].join(''));",
						"for(var i in res.diff) $('.result').append(['<div>',mineral[i],' : ',res.diff[i],'</div>'].join(''));",
						"$('.result').html();",
					"});",
				"});",
			"});",
		"</script>"].join('\n'));
		callback();

		//console.log(req._GET, req._GET.trade, listItem[req._GET.trade]
	}
});